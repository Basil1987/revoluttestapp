package com.revolut.revoluttestapp.mapper.json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.revolut.revoluttestapp.pojo.FixerData;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.Set;

/**
 * Created by vkarpenko on 21.06.17.
 */

class FixerJsonDeserializer implements JsonDeserializer<FixerData> {
    @Override
    public FixerData deserialize(JsonElement json, Type typeOfT,
                                 JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();
        JsonElement jsonBaseElement = jsonObject.get(JsonConstants.FIELD_BASE);
        JsonElement jsonDate = jsonObject.get(JsonConstants.FIELD_DATE);

        FixerData fixerData = new FixerData();
        fixerData.setBase(jsonBaseElement.getAsString());
        fixerData.setDate(jsonDate.getAsString());

        JsonObject jsonRates = (JsonObject) jsonObject.get(JsonConstants.FIELD_RATES);
        Set<Map.Entry<String, JsonElement>> entries = jsonRates.entrySet();

        for (Map.Entry<String, JsonElement> entry : entries) {
            String key = entry.getKey();
            JsonElement jsonValue = entry.getValue();

            float floatValue = jsonValue.getAsFloat();
            fixerData.putRate(key, floatValue);
        }

        return fixerData;
    }
}
