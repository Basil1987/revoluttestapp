package com.revolut.revoluttestapp.mapper.json;

/**
 * Created by vkarpenko on 21.06.17.
 */

class JsonConstants {

    static final String FIELD_BASE = "base";
    static final String FIELD_DATE = "date";
    static final String FIELD_RATES = "rates";

    private JsonConstants() {
        throw new AssertionError();
    }
}
