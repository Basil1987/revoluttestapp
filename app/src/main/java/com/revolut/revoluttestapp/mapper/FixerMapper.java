package com.revolut.revoluttestapp.mapper;

import com.revolut.revoluttestapp.pojo.FixerData;

/**
 * Created by vkarpenko on 21.06.17.
 */

public interface FixerMapper {
    FixerData mapFixerData(String data);
}
