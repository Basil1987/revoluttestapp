package com.revolut.revoluttestapp.mapper.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.revolut.revoluttestapp.mapper.FixerMapper;
import com.revolut.revoluttestapp.pojo.FixerData;

/**
 * Created by vkarpenko on 21.06.17.
 */

public class JsonFixerMapper implements FixerMapper {

    private final Gson gson;

    public JsonFixerMapper() {
        gson = new GsonBuilder()
                .registerTypeAdapter(FixerData.class, new FixerJsonDeserializer())
                .create();
    }

    @Override
    public FixerData mapFixerData(String data) {
        return gson.fromJson(data, FixerData.class);
    }
}
