package com.revolut.revoluttestapp.repository;

/**
 * Created by vkarpenko on 21.06.17.
 */

public class FixerUrlSpecification implements FixerSpecification {

    private String url;

    public FixerUrlSpecification(String url) {
        this.url = url;
    }

    @Override
    public String getFixerSpecification() {
        return url;
    }
}
