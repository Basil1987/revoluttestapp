package com.revolut.revoluttestapp.repository;

import com.revolut.revoluttestapp.pojo.FixerData;

/**
 * Created by vkarpenko on 21.06.17.
 */

public interface FixerRepository {
    FixerData get(FixerSpecification fixerSpecification);
}
