package com.revolut.revoluttestapp.repository;

import com.revolut.revoluttestapp.mapper.FixerMapper;
import com.revolut.revoluttestapp.network.Client;
import com.revolut.revoluttestapp.pojo.FixerData;

/**
 * Created by vkarpenko on 21.06.17.
 */

public class FixerRepositoryRemote implements FixerRepository {
    private Client client;
    private FixerMapper fixerMapper;

    public FixerRepositoryRemote(Client client, FixerMapper fixerMapper) {
        this.client = client;
        this.fixerMapper = fixerMapper;
    }

    @Override
    public FixerData get(FixerSpecification fixerSpecification) {
        String result = client.get(fixerSpecification.getFixerSpecification());
        return fixerMapper.mapFixerData(result);
    }
}
