package com.revolut.revoluttestapp.repository;

/**
 * Created by vkarpenko on 21.06.17.
 */

public interface FixerSpecification {
    String getFixerSpecification();
}
