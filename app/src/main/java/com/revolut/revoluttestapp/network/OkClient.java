package com.revolut.revoluttestapp.network;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by vkarpenko on 21.06.17.
 */

public class OkClient implements Client {

    private final OkHttpClient client;

    public OkClient() {
        client = new OkHttpClient();
    }

    @Override
    public String get(String url) {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response;

        try {
            response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            return responseBodyToString(responseBody);
        } catch (IOException e) {
            return null;
        }
    }

    private String responseBodyToString(ResponseBody responseBody) {
        if (responseBody == null) {
            return null;
        }

        try {
            return responseBody.string();
        } catch (IOException e) {
            return null;
        }
    }
}
