package com.revolut.revoluttestapp.network;

/**
 * Created by vkarpenko on 21.06.17.
 */

public interface Client {
    String get(String url);
}
