package com.revolut.revoluttestapp.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.revolut.revoluttestapp.R;
import com.revolut.revoluttestapp.pojo.ExchangeItemData;
import com.revolut.revoluttestapp.utils.FilterUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import butterknife.Unbinder;

/**
 * Created by vkarpenko on 22.06.17.
 */

public class ExchangeItemFragment extends Fragment {
    public static final String EXTRA_EXCHANGE_ITEM = "EXTRA_EXCHANGE_ITEM";

    private Unbinder unbinder;
    private EventListener eventListener;

    private boolean isFocused;
    private boolean isScrolled;

    private ExchangeItemData exchangeItemData;

    @BindView(R.id.value)
    EditText editTextValue;

    @BindView(R.id.curr_name_view)
    TextView currName;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof EventListener) {
            eventListener = (EventListener) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_item_exchange, container, false);
        unbinder = ButterKnife.bind(this, view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            exchangeItemData = bundle.getParcelable(EXTRA_EXCHANGE_ITEM);
        }

        fillCurrencyName();

        return view;
    }

    private void fillCurrencyName() {
        if (exchangeItemData != null && exchangeItemData.getBase() != null) {
            currName.setText(exchangeItemData.getBase());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnFocusChange(value = R.id.value)
    void onInputFieldFocusChange(boolean hasFocus) {
        this.isFocused = hasFocus;
        if (isFocused) {

            if (isScrolled) {
                isScrolled = false;
                clear();
            }

            editTextValue.setFilters(FilterUtils.getLengthLimiter());
            eventListener.onFocusObtained(exchangeItemData);
        } else {
            editTextValue.setFilters(FilterUtils.getNoLimiter());
        }
    }

    @OnTextChanged(value = R.id.value, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void afterValueChanged(Editable editable) {
        exchangeItemData.setAmountValue(editable.toString());
        onValueChangeCallback();
    }

    private void onValueChangeCallback() {
        if (eventListener != null && isFocused) {
            eventListener.onValueChange(exchangeItemData);
        }
    }

    public void setValue(ExchangeItemData exchangeItemData) {
        if (isFocused) {
            return;
        }

        editTextValue.setText(exchangeItemData.getAmountValue());
    }

    public ExchangeItemData getExchangeItemData() {
        return exchangeItemData;
    }

    public void clear() {
        if (editTextValue != null) {
            editTextValue.setText("");
        }
    }

    public void setScrolled(boolean isScrolled) {
        this.isScrolled = isScrolled;
    }

    public interface EventListener {
        void onValueChange(ExchangeItemData exchangeItemData);

        void onFocusObtained(ExchangeItemData exchangeItemData);
    }
}
