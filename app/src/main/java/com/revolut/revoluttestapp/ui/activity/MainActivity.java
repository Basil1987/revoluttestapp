package com.revolut.revoluttestapp.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.revolut.revoluttestapp.R;
import com.revolut.revoluttestapp.exchange.presenter.MainExchangePresenter;
import com.revolut.revoluttestapp.exchange.view.ExchangeView;
import com.revolut.revoluttestapp.pojo.ExchangeItemData;
import com.revolut.revoluttestapp.ui.ExchangeItemsPagerAdapter;
import com.revolut.revoluttestapp.ui.fragment.ExchangeItemFragment;
import com.revolut.revoluttestapp.utils.Constants;
import com.revolut.revoluttestapp.utils.NetworkUtils;
import com.revolut.revoluttestapp.utils.exchanger.SimpleMoneyExchanger;
import com.revolut.revoluttestapp.utils.ViewPagerListener;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.net.ConnectivityManager.CONNECTIVITY_ACTION;

public class MainActivity extends AppCompatActivity implements ExchangeView,
        ExchangeItemFragment.EventListener {

    private static final String TAG = "MainActivity";

    public static final int DELAY_MILLIS = 100;
    public static final int BOTTOM_PAGER_INITIAL_POS = 1;

    @BindView(R.id.pager_top)
    ViewPager pagerTop;

    @BindView(R.id.pager_bottom)
    ViewPager pagerBottom;

    @BindView(R.id.indicator_top)
    CirclePageIndicator indicatorTop;

    @BindView(R.id.indicator_bottom)
    CirclePageIndicator indicatorBottom;

    @BindView(R.id.snackbar_view_container)
    CoordinatorLayout coordinatorLayout;

    private MainExchangePresenter presenter;

    private Map<String, ExchangeItemFragment> activeFragments = new HashMap<>();
    private List<ViewPagerListener> vpSelectionListeners = new ArrayList<>();

    private IntentFilter intentFilter;
    private MyReceiver receiver;

    private Snackbar connectionSnackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        prepareViewPager(pagerTop, indicatorTop);
        prepareViewPager(pagerBottom, indicatorBottom);

        setPagerPosition(pagerBottom, BOTTOM_PAGER_INITIAL_POS);

        presenter = new MainExchangePresenter(this);
        presenter.setExchanger(new SimpleMoneyExchanger());

        intentFilter = new IntentFilter();
        intentFilter.addAction(CONNECTIVITY_ACTION);
        receiver = new MyReceiver();

        connectionSnackbar = Snackbar.make(coordinatorLayout, R.string.no_connection,
                Snackbar.LENGTH_INDEFINITE);
        connectionSnackbar.setActionTextColor(Color.YELLOW);
        connectionSnackbar.setAction(R.string.hide, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectionSnackbar.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.startFixerUpdates();
        registerReceiver(receiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.stopFixerUpdates();
        unregisterReceiver(receiver);
    }

    private void prepareViewPager(ViewPager viewPager, CirclePageIndicator indicator) {
        ExchangeItemsPagerAdapter pagerAdapter = new ExchangeItemsPagerAdapter(
                getSupportFragmentManager(), activeFragments);

        pagerAdapter.setData(getExchangeDataItems());

        viewPager.setAdapter(pagerAdapter);

        ViewPagerListener pagerSelectionListener = new ViewPagerSelectionListener(pagerAdapter);

        vpSelectionListeners.add(pagerSelectionListener);
        viewPager.addOnPageChangeListener(pagerSelectionListener);

        indicator.setViewPager(viewPager);
    }

    private void setPagerPosition(final ViewPager viewPager, final int position) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                viewPager.setCurrentItem(position);
            }
        }, DELAY_MILLIS);
    }

    @Override
    public void onValueChange(ExchangeItemData exchangeItemData) {
        Log.d(TAG, "onValueChange: " + exchangeItemData.getAmountValue());

        for (String id : activeFragments.keySet()) {
            ExchangeItemFragment fragment = activeFragments.get(id);

            ExchangeItemData exchangeToItem = fragment.getExchangeItemData();
            presenter.exchange(id, exchangeItemData, exchangeToItem);
        }
    }

    @Override
    public void onFocusObtained(ExchangeItemData exchangeItemData) {
        Log.d(TAG, "onFocusObtained: " + exchangeItemData.getBase());

        for (ViewPagerListener listener : vpSelectionListeners) {
            listener.setExchangeItemData(exchangeItemData);
        }
    }

    public List<ExchangeItemData> getExchangeDataItems() {
        List<ExchangeItemData> data = new ArrayList<>();
        for (String base : Constants.FIXER_ITEMS) {
            ExchangeItemData exchangeItemData = new ExchangeItemData();
            exchangeItemData.setBase(base);

            data.add(exchangeItemData);
        }
        return data;
    }

    @Override
    public void showExchangeData(String uiResultId, ExchangeItemData exchangeItemData) {
        if (activeFragments.containsKey(uiResultId)) {
            ExchangeItemFragment fragment = activeFragments.get(uiResultId);
            fragment.setValue(exchangeItemData);
        }
    }

    @Override
    public void onConnectionStateChanged(boolean isConnected) {
        if(isConnected){
            connectionSnackbar.dismiss();
        } else {
            connectionSnackbar.show();
        }
    }

    private class ViewPagerSelectionListener extends ViewPagerListener {
        private final ExchangeItemsPagerAdapter adapter;

        ViewPagerSelectionListener(ExchangeItemsPagerAdapter adapter) {
            this.adapter = adapter;
        }

        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            putFragmentByPosition(position);
        }

        private void putFragmentByPosition(int position) {
            ExchangeItemFragment currentFragment = (ExchangeItemFragment) adapter
                    .getRegisteredFragment(position);

            if(currentFragment == null){
                return;
            }

            currentFragment.setScrolled(true);

            activeFragments.put(adapter.getAdapterUniqueId(), currentFragment);

            ExchangeItemData exchangeToItem = currentFragment.getExchangeItemData();
            presenter.exchange(adapter.getAdapterUniqueId(), exchangeItemInFocus, exchangeToItem);
        }
    }

    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String actionOfIntent = intent.getAction();

            boolean isConnected = NetworkUtils.checkForInternet(context);

            if (actionOfIntent.equals(CONNECTIVITY_ACTION)) {
                Log.d(TAG, "isConnected : " + isConnected);
                presenter.onConnectionStateChanged(isConnected);
            }
        }
    }
}
