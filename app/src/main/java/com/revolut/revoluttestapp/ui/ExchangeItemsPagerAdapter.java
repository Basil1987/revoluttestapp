package com.revolut.revoluttestapp.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.revolut.revoluttestapp.pojo.ExchangeItemData;
import com.revolut.revoluttestapp.ui.fragment.ExchangeItemFragment;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by vkarpenko on 24.06.17.
 */

public class ExchangeItemsPagerAdapter extends FragmentStatePagerAdapter {
    private final String adapterUniqueId;
    private final Map<String, ExchangeItemFragment> activeFragments;

    private List<ExchangeItemData> data;

    private SparseArray<Fragment> registeredFragments = new SparseArray<>();

    public ExchangeItemsPagerAdapter(FragmentManager fm,
                                     Map<String, ExchangeItemFragment> activeFragments) {
        super(fm);
        this.activeFragments = activeFragments;
        adapterUniqueId = UUID.randomUUID().toString();
    }

    public void setData(List<ExchangeItemData> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Fragment getItem(int position) {
        ExchangeItemData exchangeItemData = data.get(position);

        ExchangeItemFragment exchangeItemFragment = new ExchangeItemFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable(ExchangeItemFragment.EXTRA_EXCHANGE_ITEM, exchangeItemData);
        exchangeItemFragment.setArguments(bundle);

        return exchangeItemFragment;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);

        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        ExchangeItemFragment currentFragment = (ExchangeItemFragment) object;
        activeFragments.put(adapterUniqueId, currentFragment);
        super.setPrimaryItem(container, position, object);
    }

    public String getAdapterUniqueId() {
        return adapterUniqueId;
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}
