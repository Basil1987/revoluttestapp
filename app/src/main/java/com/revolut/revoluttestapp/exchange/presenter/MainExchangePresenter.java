package com.revolut.revoluttestapp.exchange.presenter;

import android.util.Log;

import com.revolut.revoluttestapp.exchange.model.ExchangeModel;
import com.revolut.revoluttestapp.exchange.model.MainExchangeModel;
import com.revolut.revoluttestapp.exchange.view.ExchangeView;
import com.revolut.revoluttestapp.pojo.ExchangeItemData;
import com.revolut.revoluttestapp.pojo.FixerData;
import com.revolut.revoluttestapp.utils.Constants;
import com.revolut.revoluttestapp.utils.exchanger.Exchanger;
import com.revolut.revoluttestapp.utils.FilterUtils;

/**
 * Created by vkarpenko on 22.06.17.
 */

public class MainExchangePresenter implements ExchangePresenter {

    private static final String TAG = "MainExchangePresenter";

    private final ExchangeView exchangeView;
    private final ExchangeModel exchangeModel;

    private Exchanger exchanger;

    public MainExchangePresenter(ExchangeView exchangeView) {
        this.exchangeView = exchangeView;
        this.exchangeModel = new MainExchangeModel(this);
    }

    public void setExchanger(Exchanger exchanger) {
        this.exchanger = exchanger;
    }

    @Override
    public void startFixerUpdates() {
        exchangeModel.startDataUpdate();
    }

    @Override
    public void stopFixerUpdates() {
        exchangeModel.stopDataUpdate();
    }

    @Override
    public void exchange(String uiResultId, ExchangeItemData exchangeFromItemData,
                         ExchangeItemData exchangeToItemData) {

        FixerData fixerData = exchangeModel.getFixerDataByBase(exchangeFromItemData.getBase());

        if (fixerData != null) {
            fixerExchange(exchangeFromItemData, exchangeToItemData, fixerData);
        }

        exchangeView.showExchangeData(uiResultId, exchangeToItemData);
    }

    @Override
    public void onConnectionStateChanged(boolean isConnected) {
        if(isConnected){
            exchangeModel.forceUpdate();
        }

        exchangeView.onConnectionStateChanged(isConnected);
    }

    @SuppressWarnings("unchecked")
    private void fixerExchange(ExchangeItemData exchangeFromItemData,
                               ExchangeItemData exchangeToItemData, FixerData fixerData) {

        Float targetRate = fixerData.getRates().get(exchangeToItemData.getBase());

        if(targetRate == null){
            targetRate = Constants.DEFAULT_EXCHANGE_RATE;
        }

        if (exchanger != null) {
            String preparedExchangeValue = exchangeFromItemData.getAmountValue()
                    .replaceAll(",", "\\.");

            Float value = Float.parseFloat(preparedExchangeValue);

            exchanger.setValue(value);
            exchanger.setActionValue(targetRate);

            Float exchangedValue = (Float) exchanger.exchange();

            String formattedValue = FilterUtils.formatFloat(exchangedValue);

            Log.d(TAG, "exchangedValue : " + formattedValue);

            exchangeToItemData.setAmountValue(formattedValue);
        }
    }
}
