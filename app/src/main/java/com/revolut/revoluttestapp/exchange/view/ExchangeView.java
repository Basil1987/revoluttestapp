package com.revolut.revoluttestapp.exchange.view;

import com.revolut.revoluttestapp.pojo.ExchangeItemData;

/**
 * Created by vkarpenko on 22.06.17.
 */

public interface ExchangeView {

    void showExchangeData(String uiResultId, ExchangeItemData exchangeItemData);

    void onConnectionStateChanged(boolean isConnected);
}
