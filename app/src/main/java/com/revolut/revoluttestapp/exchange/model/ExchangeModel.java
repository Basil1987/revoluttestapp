package com.revolut.revoluttestapp.exchange.model;

import android.support.annotation.Nullable;

import com.revolut.revoluttestapp.pojo.FixerData;

/**
 * Created by vkarpenko on 22.06.17.
 */

public interface ExchangeModel {
    void startDataUpdate();

    void stopDataUpdate();

    void forceUpdate();

    @Nullable
    FixerData getFixerDataByBase(String base);
}
