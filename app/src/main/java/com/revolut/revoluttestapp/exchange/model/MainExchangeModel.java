package com.revolut.revoluttestapp.exchange.model;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.revolut.revoluttestapp.exchange.presenter.ExchangePresenter;
import com.revolut.revoluttestapp.mapper.FixerMapper;
import com.revolut.revoluttestapp.mapper.json.JsonFixerMapper;
import com.revolut.revoluttestapp.network.Client;
import com.revolut.revoluttestapp.network.OkClient;
import com.revolut.revoluttestapp.pojo.FixerData;
import com.revolut.revoluttestapp.repository.FixerRepository;
import com.revolut.revoluttestapp.repository.FixerRepositoryRemote;
import com.revolut.revoluttestapp.repository.FixerSpecification;
import com.revolut.revoluttestapp.repository.FixerUrlSpecification;
import com.revolut.revoluttestapp.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by vkarpenko on 22.06.17.
 */

public class MainExchangeModel implements ExchangeModel {

    private static final String FIXER_THREAD = "FIXER_THREAD";

    private final FixerRepository fixerRepository;

    private final List<FixerSpecification> fixerSpecifications;

    private final Handler handler;
    private UpdateTask updateTask = new UpdateTask();
    private UpdateTask updateTaskRepetitive = new UpdateTaskRepetitive();

    private ExchangePresenter presenter;//one-way communication

    private Map<String, FixerData> fixerDatabase = new ConcurrentHashMap<>();

    public MainExchangeModel(ExchangePresenter presenter) {
        this.presenter = presenter;

        handler = getBackgroundHandler();

        Client client = new OkClient();
        FixerMapper fixerMapper = new JsonFixerMapper();

        fixerRepository = new FixerRepositoryRemote(client, fixerMapper);
        fixerSpecifications = getFixerSpecifications();
    }

    @Override
    public void startDataUpdate() {
        handler.removeCallbacks(updateTaskRepetitive);
        handler.post(updateTaskRepetitive);
    }

    @Override
    public void stopDataUpdate() {
        handler.removeCallbacks(updateTaskRepetitive);
    }

    @Override
    public void forceUpdate() {
        handler.post(updateTask);
    }

    @Override
    public FixerData getFixerDataByBase(String base) {
        return fixerDatabase.get(base);
    }

    private Handler getBackgroundHandler() {
        HandlerThread handlerThread = new HandlerThread(FIXER_THREAD);
        handlerThread.start();
        return new Handler(handlerThread.getLooper());
    }

    private String getCurrencyUrl(String currencyId) {
        return String.format(Locale.getDefault(), Constants.USD_URL, currencyId);
    }

    private FixerSpecification getFixerSpecificationByCurrId(String currencyId) {
        String url = getCurrencyUrl(currencyId);
        return new FixerUrlSpecification(url);
    }

    private List<FixerSpecification> getFixerSpecifications() {
        List<FixerSpecification> fixerSpecifications = new ArrayList<>();

        for (String fixerItem : Constants.FIXER_ITEMS) {
            fixerSpecifications.add(getFixerSpecificationByCurrId(fixerItem));
        }

        return fixerSpecifications;
    }

    private class UpdateTask implements Runnable {

        private static final String TAG = "UpdateTask";

        @Override
        public void run() {
            Log.d(TAG, "fire");

            for (FixerSpecification fixerSpecification : fixerSpecifications) {
                FixerData fixerData = fixerRepository.get(fixerSpecification);

                if (fixerData != null) {
                    fixerDatabase.put(fixerData.getBase(), fixerData);
                }
            }
        }
    }

    private class UpdateTaskRepetitive extends UpdateTask {

        @Override
        public void run() {
            super.run();
            handler.postDelayed(this, Constants.REPEAT_TIME_MILLIS);
        }
    }
}
