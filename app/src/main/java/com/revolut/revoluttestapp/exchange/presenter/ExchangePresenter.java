package com.revolut.revoluttestapp.exchange.presenter;

import com.revolut.revoluttestapp.pojo.ExchangeItemData;

/**
 * Created by vkarpenko on 22.06.17.
 */

public interface ExchangePresenter {

    void startFixerUpdates();
    void stopFixerUpdates();

    void exchange(String uiResultId, ExchangeItemData exchangeFromItemData,
                  ExchangeItemData exchangeToItemData);

    void onConnectionStateChanged(boolean isConnected);
}
