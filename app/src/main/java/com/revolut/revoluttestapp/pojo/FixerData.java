package com.revolut.revoluttestapp.pojo;

import java.util.HashMap;
import java.util.Map;

public class FixerData {

    private String base;

    private String date;

    private Map<String, Float> rates;

    public FixerData() {
        rates = new HashMap<>();
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Map<String, Float> getRates() {
        return rates;
    }

    public void putRate(String rateKey, Float rateValue) {
        rates.put(rateKey, rateValue);
    }
}
