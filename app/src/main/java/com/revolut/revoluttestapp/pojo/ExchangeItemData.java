package com.revolut.revoluttestapp.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.revolut.revoluttestapp.utils.Constants;

/**
 * Created by vkarpenko on 24.06.17.
 */

public class ExchangeItemData implements Parcelable {
    private String amountValue = "0";
    private String base;

    public ExchangeItemData() {
    }

    private ExchangeItemData(Parcel in) {
        amountValue = in.readString();
        base = in.readString();
    }

    public String getAmountValue() {
        if(amountValue.isEmpty()){
            amountValue = Constants.INITIAL_EXCHANGE_ITEM_VALUE;
        }
        return amountValue;
    }

    public void setAmountValue(String amountValue) {
        this.amountValue = amountValue;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(amountValue);
        dest.writeString(base);
    }

    public static final Creator<ExchangeItemData> CREATOR = new Creator<ExchangeItemData>() {
        @Override
        public ExchangeItemData createFromParcel(Parcel in) {
            return new ExchangeItemData(in);
        }

        @Override
        public ExchangeItemData[] newArray(int size) {
            return new ExchangeItemData[size];
        }
    };
}
