package com.revolut.revoluttestapp.utils;

/**
 * Created by vkarpenko on 24.06.17.
 */

public class Constants {

    public static final String GBP = "GBP";
    public static final String EUR = "EUR";
    public static final String USD = "USD";

    public static final String[] FIXER_ITEMS = {GBP, EUR, USD};

    public static final String USD_URL = "http://api.fixer.io/latest?base=%s";
    public static final long REPEAT_TIME_MILLIS = 30000;
    public static final String INITIAL_EXCHANGE_ITEM_VALUE = "0";

    public static final int INPUT_MAX_DIGITS = 8;
    public static final Float DEFAULT_EXCHANGE_RATE = 1f;

    private Constants() {
        throw new AssertionError();
    }
}
