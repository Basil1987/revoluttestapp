package com.revolut.revoluttestapp.utils;

import android.text.InputFilter;

import java.text.DecimalFormat;

/**
 * Created by vkarpenko on 25.06.17.
 */

public class FilterUtils {

    private static DecimalFormat format = new DecimalFormat("#.##");

    private FilterUtils() {
        throw new AssertionError();
    }

    public static InputFilter[] getLengthLimiter() {
        InputFilter lengthFilter = new InputFilter.LengthFilter(Constants.INPUT_MAX_DIGITS);
        return new InputFilter[]{lengthFilter};
    }

    public static InputFilter[] getNoLimiter() {
        return new InputFilter[]{};
    }

    public static String formatFloat(Float floatValue) {
        return format.format(floatValue);
    }
}
