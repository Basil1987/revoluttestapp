package com.revolut.revoluttestapp.utils;

import android.support.v4.view.ViewPager;
import android.util.Log;

import com.revolut.revoluttestapp.pojo.ExchangeItemData;

/**
 * Created by vkarpenko on 24.06.17.
 */

public abstract class ViewPagerListener implements ViewPager.OnPageChangeListener {
    private static final String TAG = "ViewPagerTopListener";

    protected ExchangeItemData exchangeItemInFocus;

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        Log.d(TAG, "onPageSelected: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    public void setExchangeItemData(ExchangeItemData exchangeItemInFocus) {
        this.exchangeItemInFocus = exchangeItemInFocus;
    }
}
