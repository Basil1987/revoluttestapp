package com.revolut.revoluttestapp.utils.exchanger;

/**
 * Created by vkarpenko on 23.06.17.
 */

public class SimpleMoneyExchanger implements Exchanger<Float, Float> {
    private float value;
    private float multiplierRate;

    @Override
    public void setValue(Float value) {
        this.value = value;
    }

    @Override
    public void setActionValue(Float multiplierRate) {
        this.multiplierRate = multiplierRate;
    }

    @Override
    public Float exchange() {
        return value * multiplierRate;
    }
}
