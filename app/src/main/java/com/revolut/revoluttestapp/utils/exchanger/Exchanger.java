package com.revolut.revoluttestapp.utils.exchanger;

/**
 * Created by vkarpenko on 23.06.17.
 */

public interface Exchanger<E, K> {
    void setValue(E value);

    void setActionValue(K actionValue);

    E exchange();
}
